package com.aeq.microservice_2;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Thomson on 07/08/19
 * @version 1
 * @copyright Copyright (c) 2019 The Land Administration Company Inc. All rights reserved.
 * @project lava-master
 * @descrpition
 */
@FeignClient(url = "${feign.service_1.url}", name = "service1")
public interface Service1FeignClient {

    @GetMapping
    String getHelloWorld();
}
